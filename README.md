# rhedcloud-azure-vnet-type1-blueprint #


### What is this repository for? ###

* Maintain an Azure Blueprint AS CODE for the RHEDcloud type1 vnet
* Version 1.0

### Pipeline scripts ###

The Powershell scripts in the [scripts](scripts/) folder perform these various tasks:

Script Name  | Function
------------- | -------------
createVNETwS2SGw.ps1  | Creates a resource group for a Type1 VNET
exportTemplate.ps1  | Downloads the JSON representation of a resource group from the cloud
importBlueprint.ps1 | Uploads the JSON representation of a blueprint with artifacts to the cloud
exportBlueprint.ps1 | Downloads the JSON representation of a blueprint with artifacts from the cloud

The scripts in the [scripts](scripts/) folder can be run from the Pipelines page. 

![pipelines button](images/pipelines.png)

There are 5 custom pipelines that are used to run the scripts.

Pipeline Name  | Script
------------- | -------------
create-VNET | createVNETwS2SGw.ps1
export-TEMPLATE | exportTemplate.ps1
import-BLUEPRINT | importBlueprint.ps1
export-BLUEPRINT | exportBlueprint.ps1
duplicate-BLUEPRINT | importBlueprint.ps1

Press the ![run pipeline button](images/runpipeline.png) button, then choose the "master" branch:

![master branch in pipeline selection](images/masterbranch.png)

Pick one of the "custom" pipelines:

![selecting one of the custom pipelines](images/custom.png)

Fill in the variables that differ depending on which pipeline you pick. Lastly, click the ![run button](images/run.png)button.


*Warning!!! Do not run more than one pipeline at a time unless you are sure they can't interfer with each other.*


### Blueprints with Artifacts###

"Blueprints with Artifacts" are collection of json files that are used to create and assign Azure resources and roles and attach policies programatically. 

Blueprints and their artifacts are usually created in the portal. The portal doesn't export, display or otherwise provide the json representation of the blueprint and its artifacts like it does with resource groups. The only way to do this is with the Export-AzBlueprint cmdlet.

A "blueprint with artifacts" is comprised of at least two json files.

1. "Blueprint.json", in a folder with the same name as the blueprint in the "blueprint" folder.
2. A file for the artifact, with a UUID name like "d980ce66-a2c0-4982-891c-b88cc79fa655.json", in the "Artifacts" folder, in the "blueprint" folder.

There will be additional files in the "Artifacts" folder if there are additional artifacts included in the blueprint. Each artifact gets its own json file.

These files will be created by running the `export-BLUEPRINT` pipeline. If the files already exist in the repo, they will be updated. The pipeline will run the exportBlueprint.ps1 script to download the json files and then commit the changes to the repository. The commit message will contain the BITBUCKET_BUILD_NUMBER of the pipeline. Once the pipeline is finished, you can view and edit the updated blueprint definition in bitbucket or you can pull the changes to your local repo. 

Conversly, you can make changes to the json and import them into the cloud by using the `import-BLUEPRINT` pipeline. If the blueprint exists, it'll be updated and if not, it'll be created. You be able to view, edit and publish the blueprint in the portal once the pipeline has finished. 

### Templates ###

A template is ARM template which is a json file that represents a resource group. The name is the same as the resource group, with a ".json" extension, stored in the "template" folder. The file is created by running the `export-TEMPLATE` pipeline.

The template file is useful for adding an artifact to the blueprint thru the Azure portal. ARM templates can also be used to create resources without the need of a blueprint.

### Repository Variables ###

### TODO ###
TBD

### Who do I talk to? ###

* Tom Cervenka, Tod Jackson, Colin Cole, Garvin Casimir