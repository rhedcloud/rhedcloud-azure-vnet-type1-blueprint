#Example values

# things to look at on 9/7
#   Are 3 subnets needed? Where does the VPC in AWS transit into the account's network?
#   What do we need to limit subnet access?



$tenantId = $env:tenant
$svcPrincipal = $env:userName
$passwd = ConvertTo-SecureString $env:password -AsPlainText -Force
$pscredential = New-Object System.Management.Automation.PSCredential($svcPrincipal, $passwd)

$VnetName                = $env:vnet_name
$ResourceGroup           = $VnetName+"-RG"
$Location                = "East US"
$AddressSpace            = "10.66.142.0/23"                 # /23 in AWS # profile 200 in REF env.
$FESubnetName            = "Public1" 
$FESubnet                = "10.66.142.128/26"                 # /26
$PrivateName             = "Private1"
$privateSubnet           = "10.66.143.0/25"                 # /25
$GatewaySubnetName       = "GatewaySubnet"                  # Unfortunately, we can't use "Management1" as the name as Azure requires this subnet to be called "GatewaySubnet" :-(
$GatewaySubnet           = "10.66.142.0/26"                 # doesn't appear to exist in AWS CFN
$LocalNetworkGatewayName = $env:local_network_gateway_name
$LNGPublicIP             = "133.1.1.1"                      # Public IP of the Cisco/Frontend VPN Concentrator
$LocalAddressPrefixes    = '10.101.0.0/24','10.101.1.0/24'    # <-- this is the institution's private ip scheme that should be routed to Azure -- tbd for the lab CIDR
$GatewayName             = $VnetName+"GW"
$PublicIP                = $VnetName+"GWPIP"
$GatewayIPConfig         = "gwipconfig1" 
$VPNType                 = "RouteBased" 
$GatewayType             = "Vpn"
$GWSku                   = "VpnGw1" 
$ConnectionName          = $VnetName+"to"+$LocalNetworkGatewayName
$psk                     = "Emoryabc12345"                  # <-- var determined at provision time Question: Can this be determined by Azure and sent back to Provisioning Web Service

if (Get-Module -ListAvailable -Name Az) {
    Write-Host "Module exists"
    Import-Module -Name Az -Force
} else {
    Install-Module -Name Az -Force
    Import-Module -Name Az -Force
}

# Connect to Azure using a service principal user account
Connect-AzAccount -ServicePrincipal -Credential $pscredential -Tenant $tenantId

# New Resource Group that contains our management in each subscription
New-AzResourceGroup -Name $ResourceGroup -Location $Location

# Set Variables to populate the "master configuration" command
$subnet1 = New-AzVirtualNetworkSubnetConfig -Name $GatewaySubnetName -AddressPrefix $GatewaySubnet
$subnet2 = New-AzVirtualNetworkSubnetConfig -Name $FESubnetName -AddressPrefix $FEsubnet
$subnet3 = New-AzVirtualNetworkSubnetConfig -Name $PrivateName -AddressPrefix $privateSubnet

# Create the VNET with the proper subnets -- this is local to Azure resources to be provisioned.
New-AzVirtualNetwork -Name $VnetName -ResourceGroupName $ResourceGroup -Location $Location -AddressPrefix $AddressSpace -Subnet $subnet1, $subnet2, $subnet3

# Create the local network gateway -- this allows for the local network traffic to traverse back to institution's network
New-AzLocalNetworkGateway -Name $LocalNetworkGatewayName -ResourceGroupName $ResourceGroup -Location $Location -GatewayIpAddress $LNGPublicIP -AddressPrefix @($LocalAddressPrefixes)

# Get Azure Public IP Addresss for Virtual Network Gateway
$gwpip= New-AzPublicIpAddress -Name $PublicIP -ResourceGroupName $ResourceGroup -Location $Location -AllocationMethod Dynamic

# Setup variables
$vnet = Get-AzVirtualNetwork -Name $VnetName -ResourceGroupName $ResourceGroup
$subnet = Get-AzVirtualNetworkSubnetConfig -Name $GatewaySubnetName -VirtualNetwork $vnet
$gwipconfig = New-AzVirtualNetworkGatewayIpConfig -Name $GatewayIPConfig -SubnetId $subnet.Id -PublicIpAddressId $gwpip.Id

# Now create the virtual network gateway
New-AzVirtualNetworkGateway -Name $GatewayName -ResourceGroupName $ResourceGroup -Location $Location -IpConfigurations $gwipconfig -GatewayType $GatewayType -VpnType $VPNType -GatewaySku $GWSku
$vnetgwPubIP = Get-AzPublicIpAddress -Name $PublicIP -ResourceGroupName $ResourceGroup
write-host "public ip of the s2s virtual network gateway is " $vnetgwPubIP

# Create the Connection construct
$gateway1 = Get-AzVirtualNetworkGateway -Name $GatewayName -ResourceGroupName $ResourceGroup
$local = Get-AzLocalNetworkGateway -Name $LocalNetworkGatewayName -ResourceGroupName $ResourceGroup

## Initialize the Connection construct --> This command takes ~15-20 for completion
New-AzVirtualNetworkGatewayConnection -Name $ConnectionName -ResourceGroupName $ResourceGroup -Location $Location -VirtualNetworkGateway1 $gateway1 -LocalNetworkGateway2 $local -ConnectionType IPsec -RoutingWeight 10 -SharedKey $psk


## Verify the connection between on-premises & Azure is up and online
Get-AzVirtualNetworkGatewayConnection -Name $ConnectionName -ResourceGroupName $ResourceGroup
