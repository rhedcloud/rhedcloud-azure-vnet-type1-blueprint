# imports blueprint into portal
Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
Install-Module -Name Az.Blueprint

$tenantId = $env:tenant
$svcPrincipal = $env:userName
$passwd = ConvertTo-SecureString $env:password -AsPlainText -Force
$pscredential = New-Object System.Management.Automation.PSCredential($svcPrincipal, $passwd)

# Connect to Azure using a service principal user account
Connect-AzAccount -ServicePrincipal -Credential $pscredential -Tenant $tenantId

$blueprintName = $env:blueprint_name
Write-Host 'blueprintName='+$blueprintName
$blueprintPath = 'blueprint/'+$blueprintName
Import-AzBlueprintWithArtifact -Name $blueprintName -ManagementGroupId 'Dev-MG' -InputPath $blueprintPath -Force
# TODO - Publish blueprint
