#!/bin/bash
# script to run a powershell script and then update the repo if necessary

az --version
userName=$AZ_SP_NAME
export userName
tenant=$AZ_SP_TENANT
export tenant
password=$AZ_SP_SECRET
export password
echo "PWSH_SCRIPT is $1"
pwsh -File $1
ls -l -r *
# Update repo if necessary
#mods to existing files
git status --porcelain
gitstat_rc=`git status --porcelain | wc -c`
echo gitstat_rc=$gitstat_rc
#new files
gitls_rc=`git ls-files --other --exclude-standard --directory | wc -c`
echo gitls_rc=$gitls_rc
if [ $gitstat_rc -ne 0 ] || [ $gitls_rc -ne 0 ]
then
  git add *
  git commit -m "[skip ci] Updated in pipeline ${BITBUCKET_BUILD_NUMBER}"
  git push
fi
