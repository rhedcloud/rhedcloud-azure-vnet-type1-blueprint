# exports local blueprint definition into AZ
Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
Install-Module -Name Az.Blueprint

$tenantId = $env:tenant
$svcPrincipal = $env:userName
$passwd = ConvertTo-SecureString $env:password -AsPlainText -Force
$pscredential = New-Object System.Management.Automation.PSCredential($svcPrincipal, $passwd)

# Connect to Azure using a service principal user account
Connect-AzAccount -ServicePrincipal -Credential $pscredential -Tenant $tenantId


if ( $env:blueprint_version -eq 'Draft' )
{
  $bpDefinition = Get-AzBlueprint -ManagementGroupId 'DEV-MG' -Name $env:blueprint_name
}
else
{
  $bpDefinition = Get-AzBlueprint -ManagementGroupId 'DEV-MG' -Name $env:blueprint_name -Version $env:blueprint_version
}
Write-Output('bpDefinition='+$bpDefinition)
Export-AzBlueprintWithArtifact -Blueprint $bpDefinition -OutputPath 'blueprint' -Force
