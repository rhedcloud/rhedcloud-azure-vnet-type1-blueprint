# exports resource group as a template
Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
Install-Module -Name Az.Resources

$tenantId = $env:tenant
$svcPrincipal = $env:userName
$passwd = ConvertTo-SecureString $env:password -AsPlainText -Force
$pscredential = New-Object System.Management.Automation.PSCredential($svcPrincipal, $passwd)

# Connect to Azure using a service principal user account
Connect-AzAccount -ServicePrincipal -Credential $pscredential -Tenant $tenantId

$resourceGroupName = $env:resource_group_name
Export-AzResourceGroup -ResourceGroupName $resourceGroupName -Path 'template'
